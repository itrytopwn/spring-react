/**
 * Created by itryt on 21-Jan-17.
 */
/**
 * Created by itryt on 20-Jan-17.
 */
import React from 'react'
import Reflux from 'reflux'
import {Message,Button, Form} from 'semantic-ui-react'
import {Actions,UserStore} from '../../store/store.js'

class AddForm extends Reflux.Component {

    constructor(props){
        super(props);
        this.state = { formData: {} };
        this.store = UserStore;
    }

    handleSubmit = (e, { formData }) => {
        e.preventDefault();
        this.setState({ formData });
        Actions.addUsers(formData);
    }

    render() {
        const {formData} = this.state;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Group widths='equal'>
                    <Form.Input label='Username' name='username' placeholder='Username'/>
                    <Form.Input label='Password' name='password' placeholder='Password'/>
                </Form.Group>

                <Button primary type='submit'>Add</Button>

                <Message>
                    <pre>formData: {JSON.stringify(formData, null, 2)}</pre>
                </Message>

            </Form>
        );
    }
}

export default AddForm;