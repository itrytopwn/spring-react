/**
 * Created by itryt on 20-Jan-17.
 */
import React from 'react';
import {Actions,UserStore} from '../../store/store.js'
import Reflux from 'reflux';
import { Table,Button,Icon } from 'semantic-ui-react';

class List extends Reflux.Component {
    constructor(props) {
        super(props);
        this.store = UserStore;
    };
    componentDidMount(){
        Actions.loadUsers();
        // fetch(`/user/`)
        //     .then((response) => response.json())
        //     .then((data) => {
        //         this.setState({
        //             users: data
        //         });
        //     });
    }

    handleDelete = (e, user) => {
        e.preventDefault();
        Actions.deleteUser(user);
    }

    render() {
        return (
            <div className="user-list">
                <Table striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>ID</Table.HeaderCell>
                            <Table.HeaderCell>Username</Table.HeaderCell>
                            <Table.HeaderCell>Password</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {
                            this.state.users.map((user) => {
                                return   <Table.Row key={user.id}>
                                              <Table.Cell collapsing>{user.id}</Table.Cell>
                                              <Table.Cell>{user.username}</Table.Cell>
                                              <Table.Cell>{user.password}</Table.Cell>
                                              <Table.Cell collapsing><Button inverted icon color='red' onClick={(e)=>this.handleDelete(e,user)}> <Icon name='trash' /></Button> </Table.Cell>
                                         </Table.Row>
                            })
                        }
                    </Table.Body>
                </Table>
            </div>
        );
    }
}

export default List;