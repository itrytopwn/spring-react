import React, { Component } from 'react';
import List from './components/list.js'
import AddForm from './components/add.js'
import {Header,Grid } from 'semantic-ui-react'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Grid stackable columns={2}>
              <Grid.Column>
                  <Header as='h1'>
                      List of Users
                  </Header>
                  <List/>
              </Grid.Column>
              <Grid.Column>
                  <Header as='h1'>
                  Add User
                  </Header>
                  <AddForm/>
              </Grid.Column>
          </Grid>
      </div>
    );
  }
}

export default App;
