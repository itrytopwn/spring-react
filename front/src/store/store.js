/**
 * Created by itryt on 21-Jan-17.
 */
import Reflux from 'reflux'

export var Actions = Reflux.createActions([
    'loadUsers',
    'addUsers',
    'deleteUser'
]);

export class UserStore extends Reflux.Store
{
    constructor()
    {
        super();
        this.state = {users:[]};
        this.listenables = Actions;
    }

    onLoadUsers()
    {
        fetch(`/user/`)
            .then((response) => response.json())
            .then((data) => {
                this.setState({
                    users: data
                });
            });
    }

    onAddUsers(formData)
    {
        fetch('/user/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: formData.username,
                password: formData.password,
            })
        })
            .then( (response) => {
                if (response.status >= 200 && response.status < 300) {
                    Actions.loadUsers();
                } else {
                    var error = new Error(response.statusText)
                    error.response = response
                    throw error
                }
            })
    }

    onDeleteUser(user)
    {
        fetch('/user/'+ user.username, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then( (response) => {
                if (response.status >= 200 && response.status < 300) {
                    Actions.loadUsers();
                } else {
                    var error = new Error(response.statusText)
                    error.response = response
                    throw error
                }
            })
    }


}