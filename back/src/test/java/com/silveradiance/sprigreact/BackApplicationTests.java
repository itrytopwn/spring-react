package com.silveradiance.sprigreact;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BackApplicationTests {



	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	private MockMvc mockMvc;

	private String userName0 = "testyjoe";

	private String userName1 = "testyjane";

	private HttpMessageConverter mappingJackson2HttpMessageConverter;


	@Autowired
	private UserRep userRep;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
				.findAny()
				.orElse(null);

		assertNotNull("the JSON message converter must not be null",
				this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		this.userRep.deleteAllInBatch();

		userRep.save(new User(userName0, passwordEncoder.encode("password")));
		userRep.save(new User(userName1, passwordEncoder.encode("password")));

	}

	@Test
	public void userNotFound() throws Exception {
		mockMvc.perform(get("/user/mark")
				.contentType(contentType))
				.andExpect(status().isNotFound());
	}

	@Test
	public void readSingleUser() throws Exception {
		mockMvc.perform(get("/user/" + userName0))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.id", is(this.userRep.findByUsername(userName0).getId().intValue())))
				.andExpect(jsonPath("$.username", is(userName0)))
				.andExpect(jsonPath("$.password", is(this.userRep.findByUsername(userName0).getPassword())));
	}

	@Test
	public void readAllUsers() throws Exception {

		mockMvc.perform(get("/user/")
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));
	}

	@Test
	public void createUser() throws Exception {
		String userJson = json(new User(
				"testpost", "testpost"));

		this.mockMvc.perform(post("/user/")
				.contentType(contentType)
				.content(userJson))
				.andExpect(status().isCreated());
	}



	@Test
	public void deleteUser() throws Exception {
		mockMvc.perform(delete("/user/" + userName0))
				.andExpect(status().isOk());
		mockMvc.perform(get("/user/" + userName0))
				.andExpect(status().isNotFound());
	}


	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(
				o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}
}
