package com.silveradiance.sprigreact;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
/**
 * Created by itryt on 19-Jan-17.
 */

public interface UserRep extends JpaRepository<User, Long> {

    User findByUsername(String username);

    List<User> findAll();


}




